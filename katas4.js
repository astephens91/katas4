const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

let header = document.createElement("div");
header.textContent = "Kata 1";
document.body.appendChild(header);

function kata1() {
    let newElement = document.createElement("div");
    let result = gotCitiesCSV.split(",")
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result; // Don't forget to return your output!
}
kata1();

let header2 = document.createElement("div");
header2.textContent = "Kata 2";
document.body.appendChild(header2);

function kata2() {
    let newElement = document.createElement("div");
    let result = bestThing.split(" ");
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata2();

let header3 = document.createElement("div");
header3.textContent = "Kata 3";
document.body.appendChild(header3);


function kata3() {
    let newElement = document.createElement("div");
    let arrayed = gotCitiesCSV.split(",")
    let stringed = arrayed.toString()
    let result = stringed.replace(/,/g, ";")
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result; 
}
kata3();

let header4 = document.createElement("div");
header4.textContent = "Kata 4";
document.body.appendChild(header4);

function kata4(){
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.toString();
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata4();

let header5 = document.createElement("div");
header5.textContent = "Kata 5";
document.body.appendChild(header5);

function kata5(){
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.slice(0,5);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata5();

let header6 = document.createElement("div");
header6.textContent = "Kata 6";
document.body.appendChild(header6);

function kata6(){
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.slice(3);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata6();

let header7 = document.createElement("div");
header7.textContent = "Kata 7";
document.body.appendChild(header7);

function kata7(){
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.slice(2,5);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata7();

let header8 = document.createElement("div");
header8.textContent = "Kata 8";
document.body.appendChild(header8);

function kata8(){
    let newElement = document.createElement("div");
    lotrCitiesArray.splice(2, 1);
    let result = lotrCitiesArray;
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata8();

let header9 = document.createElement("div");
header9.textContent = "Kata 9";
document.body.appendChild(header9);

function kata9(){
    let newElement = document.createElement("div");
    lotrCitiesArray.splice(5, 3);
    let result = lotrCitiesArray;
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata9();

let header10 = document.createElement("div");
header10.textContent = "Kata 10";
document.body.appendChild(header10);

function kata10(){
    let newElement = document.createElement("div");
    lotrCitiesArray.splice(2, 0, "Rohan")
    let result = lotrCitiesArray;
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata10();

let header11 = document.createElement("div");
header11.textContent = "Kata 11";
document.body.appendChild(header11);

function kata11(){
    let newElement = document.createElement("div");
    lotrCitiesArray.splice(5, 1, "Deadest Marshes")
    let result = lotrCitiesArray;
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata11();

let header12 = document.createElement("div");
header12.textContent = "Kata 12";
document.body.appendChild(header12);

function kata12(){
    let newElement = document.createElement("div");
    let result = bestThing.slice(0, 14);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata12();

let header13 = document.createElement("div");
header13.textContent = "Kata 13";
document.body.appendChild(header13);

function kata13(){
    let newElement = document.createElement("div");
    let result = bestThing.slice(69);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata13();

let header14 = document.createElement("div");
header14.textContent = "Kata 14";
document.body.appendChild(header14);

function kata14(){
    let newElement = document.createElement("div");
    let result = bestThing.slice(23, 39);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata14();

let header15 = document.createElement("div");
header15.textContent = "Kata 15";
document.body.appendChild(header15);

function kata15(){
    let newElement = document.createElement("div");
    let result = bestThing.substring(69);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata15();

let header16 = document.createElement("div");
header16.textContent = "Kata 16";
document.body.appendChild(header16);

function kata16(){
    let newElement = document.createElement("div");
    let result = bestThing.substring(23, 39);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata16();

let header17 = document.createElement("div");
header17.textContent = "Kata 17";
document.body.appendChild(header17);

function kata17(){
    let newElement = document.createElement("div");
    let result = bestThing.indexOf("only")
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata17();

let header18 = document.createElement("div");
header18.textContent = "Kata 18";
document.body.appendChild(header18);

function kata18(){
    let newElement = document.createElement("div");
    let result = bestThing.lastIndexOf("bit")
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata18();

let header19 = document.createElement("div");
header19.textContent = "Kata 19";
document.body.appendChild(header19);


//This one probably could've been done neater but it works so I didn't question it
function kata19(){
    let newElement = document.createElement("div");
    let arrayed = gotCitiesCSV.split(",");
    let pattern = 'ee';
    let pattern2 =  'aa';
    let ee = arrayed.filter(function (str) { return str.includes(pattern); });
    let aa = arrayed.filter(function (str) { return str.includes(pattern2); });
    let result = ee.concat(aa);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata19();

let header20 = document.createElement("div");
header20.textContent = "Kata 20";
document.body.appendChild(header20);

function kata20(){
    let newElement = document.createElement("div");
    let pattern = 'or';
    let result = lotrCitiesArray.filter(function (str) { return str.includes(pattern)});
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata20();

let header21 = document.createElement("div");
header21.textContent = "Kata 21";
document.body.appendChild(header21);

function kata21(){
    let newElement = document.createElement("div");
    let arrayed = bestThing.split(" ")
    let result = arrayed.filter(function (str) {return str.startsWith("b")})
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata21();

let header22 = document.createElement("div");
header22.textContent = "Kata 22";
document.body.appendChild(header22);

function kata22(){
    let newElement = document.createElement("div");
    if (lotrCitiesArray.includes("Mirkwood")){
        result = "Yes"
    }
    else {
        result = "No"
    }
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata22();

let header23 = document.createElement("div");
header23.textContent = "Kata 23";
document.body.appendChild(header23);

function kata23(){
    let newElement = document.createElement("div");
    if (lotrCitiesArray.includes("Hollywood")){
        result = "Yes"
    }
    else {
        result = "No"
    }
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata23();

let header24 = document.createElement("div");
header24.textContent = "Kata 24";
document.body.appendChild(header24);

function kata24(){
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.indexOf("Mirkwood");
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata24();

let header25 = document.createElement("div");
header25.textContent = "Kata 25";
document.body.appendChild(header25);

function kata25(){
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.filter(function (str) { return str.includes(" ")});
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata25();

let header26 = document.createElement("div");
header26.textContent = "Kata 26";
document.body.appendChild(header26);

function kata26(){
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.reverse();
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata26();

let header27 = document.createElement("div");
header27.textContent = "Kata 27";
document.body.appendChild(header27);

function kata27(){
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.sort();
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata27();

let header28 = document.createElement("div");
header28.textContent = "Kata 28";
document.body.appendChild(header28);

function kata28(){
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.sort(function(a, b){
        return b.length - a.length
    });
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata28();

let header29 = document.createElement("div");
header29.textContent = "Kata 29";
document.body.appendChild(header29);

function kata29(){
    let newElement = document.createElement("div");
    lotrCitiesArray.pop()
    let result = lotrCitiesArray
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata29();

let header30 = document.createElement("div");
header30.textContent = "Kata 30";
document.body.appendChild(header30);

function kata30(){
    let newElement = document.createElement("div");
    lotrCitiesArray.push("Rohan")
    let result = lotrCitiesArray;
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata30();

let header31 = document.createElement("div");
header31.textContent = "Kata 31";
document.body.appendChild(header31);

function kata31(){
    let newElement = document.createElement("div");
    lotrCitiesArray.shift()
    let result = lotrCitiesArray
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata31();

let header32 = document.createElement("div");
header32.textContent = "Kata 32";
document.body.appendChild(header32);

function kata32(){
    let newElement = document.createElement("div");
    lotrCitiesArray.unshift("Deadest Marshes")
    let result = lotrCitiesArray;
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata32();